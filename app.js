// app.js
var express =       require('express');
var subdomain =     require('express-subdomain');
var mongoose =      require('mongoose');
var bodyparser =    require('body-parser');
var exphbs =        require('express-handlebars');
var ejs =           require('ejs');
var morgan =        require('morgan');
var cookieParser =  require('cookie-parser');
var passport =      require('passport');
var flash =         require('connect-flash');
var session =       require('express-session');

var app = express();
var port = process.env.PORT || 8000;

var db = mongoose.connect('mongodb://localhost/blog-nodejs');
var models = require('./models/allModels');

//var passportConfig = require('./config/passport.js');
require('./config/passport')(passport);

// middleware ======================================
app.use('/client', express.static('client'));
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(cookieParser()); // read cookies (needed for auth)
app.use(morgan('dev'));

// required for passport
app.use(session({ secret: 'georgiaisawesome', saveUninitialized: true, resave: true })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


// view engine =====================================
//app.engine('.hbs', exphbs({defaultLayout: 'single', extname: '.hbs'}));
//app.set('view engine', '.hbs');

//app.engine('html', ejs.renderFile);
//app.set('view engine', 'html');
//app.set('view engine', 'ejs');

// routes ============================================
var apiRouter = require('./routes/api')(app, passport);

app.get('/isUserLoggedIn', function(req, res){

	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({ isUserLoggedIn: req.isAuthenticated() }));

});

app.post('/login', passport.authenticate('local-login', {
	successRedirect : '/blog', // redirect to the secure profile section
	failureRedirect : '/contact', // redirect back to the signup page if there is an error
	failureFlash : true // allow flash messages
}));

app.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});

app.post('/signup', passport.authenticate('local-signup', {
	successRedirect : '/blog', // redirect to the secure profile section
	failureRedirect : '/', // redirect back to the signup page if there is an error
	failureFlash : true // allow flash messages
}), function(req, res){
	// Set a flash message by passing the key, followed by the value, to req.flash().
	req.flash('info', 'Flash is back!');
});


var mainRouter = require('./routes/web')(app);


// launch ============================================
app.listen(port, function(){

    console.log('Listening on PORT: ' + port);

});