angular.module('mainApp').controller('blogCtrl', blogCtrl);

blogCtrl.$inject = ['$scope', 'dataService'];

function blogCtrl($scope, dataService){

	//$scope.secondaryUrl = 'client/main-app/views/secondary.html';

	$scope.secondaryDisplay = {
		followUs: true,
		popularPosts: true,
		categories: true,
		search: true,
		recentComments: true,
		categoryDropDown: true,
		tags: true
	};

	dataService.getPosts()
		.then(function getPostsSuccess(response){
			console.log('getPosts() Success');
			$scope.posts = response.data;
		},
		function getPostsFail(response){
			console.log('Houston we have a fail.');
		});

	dataService.getTags()
		.then(function getTagsSuccess(response){
			console.log('getTags() Success');
			$scope.tags = response.data;
		},
		function getTagsFail(response){
			console.log('getTags() Houston we have a fail.');
		});

	dataService.getCategories()
		.then(function getCategoriesSuccess(response){
			console.log('getCategories() Success');
			$scope.categories = response.data;
		},
		function getCategoriesFail(response){
			console.log('getCategories() Houston we have a fail.');
		});

}//END blogCtrl