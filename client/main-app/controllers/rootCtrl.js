angular.module('mainApp').controller('rootCtrl', rootCtrl);

rootCtrl.$inject = ['$scope', '$stateParams', 'dataService', 'authService'];

function rootCtrl($scope, $stateParams, dataService, authService){

	authService.isUserAuthenticated()
		.then(function onSuccess(response){
			debugger;
			$scope.userLoggedIn = response.data.isUserLoggedIn;

		}, function onFailure(response){


		});

	//$animate.enabled(false);

	$scope.carouselInterval = 2500;
	$scope.noWrapSlides = false;

	$scope.slides = [
		{
			active: true,
			url: 'client/images/monkey-white-1920x600.png',
			text: 'sample text'
		},
		{
			active: true,
			url: 'client/images/binary-white-black-1920x600.jpg',
			text: 'sample text'
		},
		{
			active: true,
			url: 'client/images/binary-green-blue-1920x600.jpg',
			text: 'sample text'
		}
	];

}//END rootCtrl