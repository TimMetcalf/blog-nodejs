var mainApp = angular.module('mainApp', ['ui.router', 'ui.bootstrap', 'ngAnimate']);


//CONFIGURE
mainApp.config(function($stateProvider, $urlRouterProvider, $locationProvider){

	var baseUrl = 'client/main-app/views/';

	// For any unmatched url, redirect to /state1
	$urlRouterProvider.otherwise("/");

	// Now set up the states
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: baseUrl + 'home.html',
			controller: function(){},
			authenticate: false
		})
		.state('blog-limit', {
			url: '/blog/:filterType/:filterBy/:limit',
			templateUrl: baseUrl + 'blog.html',
			controller: 'blogCtrl'
		})
		.state('blog-filterby', {
			url: '/blog/:filterType/:filterBy',
			templateUrl: baseUrl + 'blog.html',
			controller: 'blogCtrl'
		})
		.state('post', {
			url: '/blog/:slug',
			templateUrl: baseUrl + 'post.html',
			controller: 'postCtrl',
			resolve:{
				post: ['$stateParams','$http', function getPost($stateParams, $http){

					// This line is updated to return the promise
					return $http({
							method: 'GET',
							url: 'isUserLoggedIn'
						})
						.then(
							function onSuccess(response){
								return response.data;
							},
							function onFail(){
								return {};
							}
					);
				}]
			},
			authenticate: true
		})
		.state('blog-all', {
			url: '/blog',
			templateUrl: baseUrl + 'blog.html',
			controller: 'blogCtrl',
			authenticate: false
		})
		.state('contact', {
			url: '/contact',
			templateUrl: baseUrl + 'contact.html'
		})
		.state('author-bio', {
			url: '/author-bio',
			templateUrl: 'author.html'
		})
		.state('login', {
			url: '/login',
			templateUrl: baseUrl + 'login.html'
		})
		.state('signup', {
			url: '/signup',
			templateUrl: baseUrl + 'signup.html'
		});

	// use the HTML5 History API
	$locationProvider.html5Mode(true);
});

//RUN
mainApp.run(['$rootScope', '$state', 'authService', function ($rootScope, $state, authService) {

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){

		if(toState.authenticate && !authService.isUserAuthenticated()) {

			// User isn’t authenticated
			$state.transitionTo("login");
			event.preventDefault();
		}
	});

}]);

//DIRECTIVES
mainApp.directive('tmDateFormat', function tmDateFormat(){

	return{
		restrict: 'A',
		link: function(scope, element, attr){
			//debugger;
			var date = new Date(attr.tmDateFormat);
			element.text(date.toLocaleString());

		}
	};

});
