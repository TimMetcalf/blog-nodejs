angular.module('mainApp').factory('authService', authService);

//inject dependencies that authService needs here
authService.$inject = ['$http'];

function authService($http){

	//public members
	return {

		isUserAuthenticated: isUserAuthenticated,
		authenticateUser: authenticateUser,
		logOutUser: logOutUser
	};

	function isUserAuthenticated(){

		return $http({
			method: 'GET',
			url: 'isUserLoggedIn'
		});
	}

	function logOutUser(){

		return $http({
			method: 'GET',
			url: 'api/posts',
			cache: true
		});
	}

	function authenticateUser(){

		return $http({
			method: 'GET',
			url: 'api/tags'
		});
	}

}