angular.module('mainApp').factory('dataService', dataService);

//inject dependencies that dataService needs here
dataService.$inject = ['$http'];

function dataService($http){

	//public members
	return {

		getPosts: getPosts,
		getTags: getTags,
		getCategories: getCategories,
		getPostBySlug: getPostBySlug
	};

	function getPostBySlug(slug){

		return $http({
			method: 'GET',
			url: 'api/posts/' + slug
		});
	}

	function getPosts(){

			return $http({
				method: 'GET',
				url: 'api/posts',
				cache: true
			});
	}

	function getTags(){

		return $http({
			method: 'GET',
			url: 'api/tags'
		});
	}

	function getCategories() {

		return $http({
			method: 'GET',
			url: 'api/categories'
		});
	}

}