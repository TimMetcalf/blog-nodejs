var mongoose = require('mongoose');

var Book = require('./bookModel');
var Tag = require('./tagModel');
var Category = require('./categoryModel');
var User = require('./userModel');
var Post = require('./postModel');

module.exports = {
	Book: Book,
	Tag: Tag,
	Category: Category,
	User: User,
	Post: Post
};