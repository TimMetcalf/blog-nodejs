var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categoryModel = new Schema({
	_id:    Number,
	name:   String,
	slug:   String,
	value:  String
});

module.exports = mongoose.model("Category", categoryModel);