var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postModel = new Schema({
	_id:            Number,
	title:          { type: String },
	slug:           { type: String },
	author:         { type: mongoose.Schema.Types.Number, ref: 'User' },
	content:        { type: String },
	teaserContent:  { type: String },
	dateCreated:    { type: Date, default: Date.now },
	dateModified:   { type: Date, default: null },
	tags:           [ {type: mongoose.Schema.Types.Number, ref: 'Tag'} ],
	mainImageUrl:   { type: String }
});

module.exports = mongoose.model("Post", postModel);