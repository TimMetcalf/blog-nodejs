var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagModel = new Schema({
	_id:    Number,
	name:   String,
	slug:   String,
	value:  String
});

module.exports = mongoose.model("Tag", tagModel);