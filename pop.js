var mongoose = require('mongoose');
var models = require('./models/allModels');
var EventEmitter = require("events").EventEmitter;

//SETUP EVENT EMITTER (used to prevent callback hell)
var ee = new EventEmitter();

//CONNECT TO MONGODB
var db = mongoose.connect('mongodb://localhost/blog-nodejs');

//FETCH DATA
var tagDocs =       require('./seed/tags.json');
var categoryDocs =  require('./seed/categories.json');
var userDocs =      require('./seed/users.json');
var postDocs =      require('./seed/posts.json');


//BEGIN POPULATING
//TAGS
models.Tag.create( tagDocs, function(err, tags){
	if(!err){
		tags.forEach(function(element, index, array){
			console.log(element.name + ' has been inserted.');
		});
	}else{
		console.log(err)
	}
	//
	ee.emit('tagsFinished');
});


//CATEGORIES
ee.on("tagsFinished", function () {
	console.log("CATEGORIES FINISHED!!!");
	models.Category.create( categoryDocs, function(err, categories){
		if(!err){
			categories.forEach(function(element, index, array){
				console.log(element.name + ' has been inserted.');
			});
		}else{
			console.log(err)
		}
		//
		ee.emit('categoriesFinished');
	});
});


//USERS
ee.on("categoriesFinished", function () {
	console.log("CATEGORIES FINISHED!!!");
	models.User.create( userDocs, function(err, users){
		if(!err){
			users.forEach(function(element, index, array){
				console.log(element.userName + ' has been inserted.');
			});
		}else{
			console.log(err)
		}
		//
		ee.emit('usersFinished');
	});
});


//POSTS
ee.on("usersFinished", function () {
	console.log('USERS FINISHED!!!');
	models.Post.create(postDocs, function (err, posts) {
		if (!err) {
			posts.forEach(function (element, index, array) {
				console.log(element.slug + ' has been inserted.');
			});
		} else {
			console.log(err)
		}
		//
		ee.emit('postsFinished');
	});
});


//END POPULATING
ee.on('postsFinished', function(){
	AllDone();
});


//this should be the very last function called in this script
function AllDone(){
	console.log('pop.js is finished...now exiting.');
	console.log();
	process.exit()
}