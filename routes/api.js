var express =   require('express');
var subdomain = require('express-subdomain');

var models =    require('../models/allModels');


var routes = function(app, passport) {

	var apiRoutes = express.Router();

	var bookRouter =      require('./bookRoutes')(models.Book);
	var userRouter =      require('./userRoutes')(models.User);
	var postRouter =      require('./postRoutes')(models.Post);
	var tagRouter =       require('./tagRoutes')(models.Tag, passport);
	var categoryRouter =  require('./categoryRoutes')(models.Category);


	app.use('/api/books', bookRouter);
	app.use('/api/users', userRouter);
	app.use('/api/posts', postRouter);
	app.use('/api/tags', tagRouter);
	app.use('/api/categories', categoryRouter);

	//app.use(subdomain('api', bookRouter));
	//app.use(subdomain('api', userRouter));
	//app.use(subdomain('api', postRouter));


	return apiRoutes;
};

module.exports = routes;