var express = require('express');

var routes = function(Category){

	var categoryRouter = express.Router();

	categoryRouter.route('/')
		.post(function(req, res){

			var category = new Category(req.body);
			category.save();
			res.status(201).send(category);
		})
		.get(function(req, res){
			Category.find(function(err, categories){
				if(err){
					res.status(500).send(err);
				}else{
					res.json(categories);
				}
			});
		});

	categoryRouter.use('/:categoryId', function(req, res, next){

		Category.findById(req.params.categoryId, function(err, category){

			if(err){
				res.status(500).send(err);
			}else if(category){
				req.category = category;
				next();
			}else{
				res.status(404).send('No Category Found.');
			}
		});

	});

	categoryRouter.route('/:categoryId')
		.get(function(req, res){

			res.json(req.category);
		})
		.put(function(req, res){

			req.category.name = req.body.name;
			req.category.slug = req.body.slug;
			req.category.value = req.body.value;

			req.category.save();
			res.json(req.category);
		})
		.patch(function(req, res){

			if(req.category._id)
				delete req.body._id;

			for(var p in req.body){
				req.category[p] = req.body[p];
			}

			req.category.save(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.json(req.category);
				}
			})

		})
		.delete(function(req, res){

			req.category.remove(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.status(204).send("Category Removed");
				}
			});

		});


	return categoryRouter;
};

module.exports = routes;