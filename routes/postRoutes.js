var express = require('express');

module.exports = function(Post, passport){

	var postRouter = express.Router();

	postRouter.route('/')
		.post(isLoggedIn, function(req, res){

			var post = new Post(req.body);
			post.save();
			res.status(201).send(post);
		})
		.get(function(req, res){
			Post.find(function(err, posts){
				if(err){
					res.status(500).send(err);
				}else {
					var promise = Post.populate(posts, {path: 'author'});
					promise.then(function onSuccess() {
						res.json(posts);
					}).end();
					//res.json(posts);
				}
			});
		});

	postRouter.use('/:postId', function(req, res, next){

		var postId = req.params.postId;

		if(isNaN(postId)){

			Post.findOne({ slug : postId }, function(err, post){

				if(err){
					res.status(500).send(err);
				}else if(post){
					req.post = post;
					next();
				}else{
					res.status(404).send('No Post Found.');
				}

			});

		}else{

			Post.findById(req.params.postId, function(err, post){

				if(err){
					res.status(500).send(err);
				}else if(post){
					req.post = post;
					next();
				}else{
					res.status(404).send('No Post Found.');
				}

			});

		}

	});

	postRouter.route('/:postId')
		.get(function(req, res){

			res.json(req.post);
		})
		.put(isLoggedIn, function(req, res){

			req.post.title = req.body.title;
			req.post.slug = req.body.slug;
			req.post.author = req.body.author;

			req.post.content = req.body.content;
			req.post.teaserContent = req.body.teaserContent;
			req.post.dateModifed = Date.now;
			req.post.tags = req.body.tags;

			req.post.save();
			res.json(req.post);
		})
		.patch(function(req, res){

			if(req.post._id)
				delete req.body._id;

			for(var p in req.body){
				req.post[p] = req.body[p];
			}

			req.post.save(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.json(req.post);
				}
			})

		})
		.delete(isLoggedIn, function(req, res){

			req.post.remove(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.status(204).send("Removed");
				}
			});

		});

	function isLoggedIn(req, res, next){
		debugger;
		// if user is authenticated in the session, carry on
		if (req.isAuthenticated())
			return next();

		// if they aren't redirect them to the home page
		//res.redirect('/login');
		res.status(403);
		res.send('Denied');
	}

	return postRouter;
};