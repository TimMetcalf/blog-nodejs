var express = require('express');

var routes = function(Tag, passport){

	var tagRouter = express.Router();

	tagRouter.route('/')
		.post(function(req, res){

			var tag = new Tag(req.body);
			tag.save();
			res.status(201).send(tag);

		})
		.get(function(req, res){

				Tag.find(function(err, tags){
					if(err){
						res.status(500).send(err);
					}else{
						res.json(tags);
					}
				});

		});

	tagRouter.use('/:tagId', function(req, res, next){

		Tag.findById(req.params.tagId, function(err, tag){

			if(err){
				res.status(500).send(err);
			}else if(tag){
				req.tag = tag;
				next();
			}else{
				res.status(404).send('No Tag Found.');
			}

		});

	});

	tagRouter.route('/:tagId')
		.get(function(req, res){

			res.json(req.tag);
		})
		.put(function(req, res){

			req.tag.name = req.body.name;
			req.tag.slug = req.body.slug;
			req.tag.value = req.body.value;

			req.tag.save();
			res.json(req.tag);
		})
		.patch(function(req, res){

			if(req.tag._id)
				delete req.body._id;

			for(var p in req.body){
				req.tag[p] = req.body[p];
			}

			req.tag.save(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.json(req.tag);
				}
			})

		})
		.delete(function(req, res){

			req.tag.remove(function(err){

				if(err){
					res.status(500).send(err);
				}else{
					res.status(204).send("Tag Removed");
				}
			});

		});


	return tagRouter;
};

module.exports = routes;