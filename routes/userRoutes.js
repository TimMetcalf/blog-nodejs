var express = require('express');

var routes = function(User){

    var userRouter = express.Router();

    userRouter.route('/')
        .post(function(req, res){

            var user = new User(req.body);
            user.save();
            res.status(201).send(user);
        })
        .get(function(req, res){
            User.find(function(err, users){
                if(err){
                    res.status(500).send(err);
                }else{
                    res.json(users);
                }
            });
        });

    userRouter.use('/:userId', function(req, res, next){

        User.findById(req.params.userId, function(err, user){

            if(err){
                res.status(500).send(err);
            }else if(user){
                req.user = user;
                next();
            }else{
                res.status(404).send('No User Found.');
            }

        });

    });

    userRouter.route('/:userId')
        .get(function(req, res){

            res.json(req.user);
            //Book.findById(req.params.bookId, function(err, book){
            //
            //    if(err){
            //        res.status(500).send(err);
            //    }else{
            //        res.json(book);
            //    }
            //
            //});

        })
        .put(function(req, res){

            req.user.name = req.body.name;
            req.user.userName = req.body.userName;
            req.user.save();
            res.json(req.user);

        })
        .patch(function(req, res){

            if(req.user._id)
                delete req.body._id;

            for(var p in req.body){
                req.user[p] = req.body[p];
            }

            req.user.save(function(err){

                if(err){
                    res.status(500).send(err);
                }else{
                    res.json(req.user);
                }
            })

        })
        .delete(function(req, res){

            req.user.remove(function(err){

                if(err){
                    res.status(500).send(err);
                }else{
                    res.status(204).send("Removed");
                }
            });

        });


    return userRouter;
};

module.exports = routes;