var express = require('express');
var path = require('path');
//var passport = require('passport');

var routes = function(app) {

	var webRoutes = express.Router();

	webRoutes.get('*', function(req, res){
		res.sendFile(path.join(__dirname, '../client', 'index.html'));
	});

	app.use('/', webRoutes);
	return webRoutes;
};

module.exports = routes;